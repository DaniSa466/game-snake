// Fill out your copyright notice in the Description page of Project Settings.

#include "Food.h"
#include "SnakeBase.h"
#include "FoodSpawner.h"
#include "SnakeGameGameModeBase.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			AActor* foundActor = UGameplayStatics::GetActorOfClass(GetWorld(), AFoodSpawner::StaticClass());
			auto SpawnFoodCaller = Cast<AFoodSpawner>(foundActor);

			if (Snake->SnakeElements.Num() == 10)
			{
				UWorld* World = GetWorld();
				if (World != nullptr)
				{
					ASnakeGameGameModeBase* GameMode = Cast<ASnakeGameGameModeBase>(UGameplayStatics::GetGameMode(World));
					if (GameMode != nullptr)
					{
						GameMode->GameOver(EGameOverReason::WinGame);
						Snake->Destroy();
					}
				}
			}

			SpawnFoodCaller->SpawnFood();

			this->Destroy();
		}
	}
}


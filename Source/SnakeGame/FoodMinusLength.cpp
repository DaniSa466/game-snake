// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodMinusLength.h"
#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFoodMinusLength::AFoodMinusLength()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodMinusLength::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodMinusLength::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodMinusLength::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->DeleteSnakeElement();
		}
	}
	Super::Interact(Interactor, bIsHead);
}


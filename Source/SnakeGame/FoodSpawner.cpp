// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSpawner.h"
#include "GameInstanceBase.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
AFoodSpawner::AFoodSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodSpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodSpawner::SpawnFood()
{
	FTransform newTransform = GetActorTransform();
	newTransform.SetLocation(GetRandomCoordsForRange());

	if (FoodClasses.Num() == 0) return;
	
	/*
	FoodClasses[0] is FoodPlusLenght, FoodClasses[1] is FoodMinusLength.
	this constraction is responsible for spawning randomizing. PlusLength has
	75% chance to spawn, while MinusLength has 25% chance to spawn.
	*/
	int num;
	num = FMath::RandRange(0, 3);
	TSubclassOf<AFood> ClassToSpawn;

	if (num == 3) ClassToSpawn = FoodClasses[1];
	else ClassToSpawn = FoodClasses[0];
	AFood* newFood = GetWorld()->SpawnActor<AFood>(ClassToSpawn, newTransform);
}

FVector AFoodSpawner::GetRandomCoordsForRange() const
{
	UGameInstanceBase* GI = Cast<UGameInstanceBase>(GetGameInstance());
	if (GI == nullptr) return FVector(0, 0, 0);
	FVector var1 = GI->TopRightPlace;
	FVector var2 = GI->BottomLeftPlace;

	return FVector(FMath::RandRange(var1.X, var2.X), FMath::RandRange(var1.Y, var2.Y), 0);
}


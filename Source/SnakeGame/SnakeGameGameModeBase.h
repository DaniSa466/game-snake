// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameGameModeBase.generated.h"

class UGameOverUI;

UENUM(BlueprintType)
enum class EGameOverReason :uint8
{
	LoseGame = 0 UMETA(DisplayName = "Lose Game"),
	WinGame = 1 UMETA(DisplayName = "Win Game")
};

UCLASS()
class SNAKEGAME_API ASnakeGameGameModeBase : public AGameModeBase
{
public:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TSubclassOf<UGameOverUI> GameOverUIClass;

public:
	UFUNCTION()
	void GameOver(EGameOverReason Reason = EGameOverReason::LoseGame);

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TMap<EGameOverReason, FString> GameOverStrings;

private:
	UPROPERTY()
	UGameOverUI* GameOverUI = nullptr;


	GENERATED_BODY()
	
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreatSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreatSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMovementDirection != EMovementDirection::DOWN && SnakeActor->Moving == true)
		{
			SnakeActor->LastMovementDirection = EMovementDirection::UP;
			SnakeActor->Moving = false;	
		}
		else if (value < 0 && SnakeActor->LastMovementDirection != EMovementDirection::UP && SnakeActor->Moving == true)
		{
			SnakeActor->LastMovementDirection = EMovementDirection::DOWN;
			SnakeActor->Moving = false;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (value > 0 && SnakeActor->LastMovementDirection != EMovementDirection::LEFT && SnakeActor->Moving == true)
	{
		SnakeActor->LastMovementDirection = EMovementDirection::RIGHT;
		SnakeActor->Moving = false;
	}
	else if (value < 0 && SnakeActor->LastMovementDirection != EMovementDirection::RIGHT && SnakeActor->Moving == true)
	{
		SnakeActor->LastMovementDirection = EMovementDirection::LEFT;
		SnakeActor->Moving = false;
	}
}


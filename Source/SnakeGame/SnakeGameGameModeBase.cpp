// Copyright Epic Games, Inc. All Rights Reserved.


#include "SnakeGameGameModeBase.h"
#include "GameOverUI.h"
#include "Kismet/GameplayStatics.h"

void ASnakeGameGameModeBase::GameOver(EGameOverReason Reason)
{
	const UWorld* GameWorld = GetWorld();
	if (GameOverStrings.Num() < 1) 
	{
		UE_LOG(LogTemp, Warning, TEXT("EndGameStrings is empty"));
		return;
	}
	if (GameOverUIClass == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("GameOverUIClass = null"));
		return;
	}
	if (GameWorld == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Here is no World"));
		return;
	}

	APlayerController* PC = UGameplayStatics::GetPlayerController(GameWorld, 0);
	GameOverUI = Cast<UGameOverUI>(CreateWidget(PC, GameOverUIClass));

	if (GameOverUI == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Unavaileble to initialize GameOverUI"));
		return;
	}

	FString* Message = GameOverStrings.Find(Reason);
	if (Message == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Message is empty"));
		return;
	}

	GameOverUI->GameOverText = *Message;
	GameOverUI->AddToViewport();
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameOverUI.generated.h"

UCLASS()
class SNAKEGAME_API UGameOverUI : public UUserWidget
{
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString GameOverText = "Default";

	GENERATED_BODY()
	
};

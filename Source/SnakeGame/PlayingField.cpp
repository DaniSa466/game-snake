// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayingField.h"
#include "SnakeGameGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeBase.h"

// Sets default values
APlayingField::APlayingField()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APlayingField::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayingField::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlayingField::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			UWorld* World = GetWorld();
			if (World != nullptr)
			{
				ASnakeGameGameModeBase* GameMode = Cast<ASnakeGameGameModeBase>(UGameplayStatics::GetGameMode(World));
				if (GameMode != nullptr)
				{
					GameMode->GameOver(EGameOverReason::LoseGame);
				}
			}

			Snake->Destroy();
		}
	}
}

